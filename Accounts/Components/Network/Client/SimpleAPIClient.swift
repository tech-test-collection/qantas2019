//
//  SimpleAPIClient.swift
//  Accounts
//
//  We let Alamofire do most of the boring lifting.
//  This includes obvious things like making sure the JSON decoding is done on background threading
//  And less obvious things like in the real world inject auth plugins for bearer token
//  It also makes the code more readable by lifting out boiler plate, and provides a testing framework.
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation
import Alamofire

class SimpleAPIClient: APIClient {
    let environment: APIEnvironment
    
    public init(environment: APIEnvironment) {
        self.environment = environment
    }
    
    func fetchAccounts(completionHandler: @escaping (Result<AccountResponse, AFError>) -> Void) {
        guard let url = environment.accountListURL else {
            completionHandler(Result<AccountResponse, AFError>.failure(.sessionDeinitialized))
            return
        }
        
        AF.request(url, method: .get)
        .responseDecodable { (response: DataResponse<AccountResponse, AFError>) in
            completionHandler(response.result)
        }
    }
    
    func fetchTransactions(completionHandler: @escaping (Result<TransactionResponse, AFError>) -> Void) {
        guard let url = environment.transactionListURL else {
            completionHandler(Result<TransactionResponse, AFError>.failure(.sessionDeinitialized))
            return
        }
        
        AF.request(url, method: .get)
        .responseDecodable { (response: DataResponse<TransactionResponse, AFError>) in
            completionHandler(response.result)
        }
    }
}
