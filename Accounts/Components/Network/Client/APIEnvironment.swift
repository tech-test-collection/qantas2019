//
//  APIEnvironment.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

enum APIEnvironment {
    case demo
}

extension APIEnvironment {
    var accountListURL: URL? {
        return URL(string: "https://jsonstorage.net/api/items/d97f27b5-caba-4cc8-9f5d-32b0208ec7f0")
    }
    
    var transactionListURL: URL? {
        return URL(string: "https://jsonstorage.net/api/items/7a8a340d-b450-4adc-bbba-f6b4c8cdbc09")
    }
}
