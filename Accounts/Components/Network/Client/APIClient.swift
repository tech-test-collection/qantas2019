//
//  ApiClient.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation
import Alamofire

protocol APIClient {
    // fetches the list of accounts, ordinarily it'd require some sort of session present
    func fetchAccounts(completionHandler: @escaping (Result<AccountResponse, AFError>) -> Void)
    // ordinarily there'd be some sort of account ID param, except our mock feed is fixed
    func fetchTransactions(completionHandler: @escaping (Result<TransactionResponse, AFError>) -> Void)
}
