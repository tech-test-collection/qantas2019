// Address.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let address = try? newJSONDecoder().decode(Address.self, from: jsonData)

import Foundation

// MARK: - Address
struct Address: Codable {
    let address, city, region, country, postcode: String?
    let latitude, longitude: Double?
}
