// AccountResponse.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let accountResponse = try? newJSONDecoder().decode(AccountResponse.self, from: jsonData)

import Foundation

// MARK: - AccountResponse
struct AccountResponse: Codable {
    let accounts: [Account]
}
