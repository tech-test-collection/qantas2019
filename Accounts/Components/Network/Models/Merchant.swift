// Merchant.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let merchant = try? newJSONDecoder().decode(Merchant.self, from: jsonData)

import Foundation

// MARK: - Merchant
struct Merchant: Codable {
    let name, updated: String?
    let address: Address?
}
