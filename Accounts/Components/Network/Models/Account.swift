// Account.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let account = try? newJSONDecoder().decode(Account.self, from: jsonData)

import Foundation

// MARK: - Account
struct Account: Codable {
    let id: Int
    let name: String
    let balance, availableBalance: Double
    let currency: String

    enum CodingKeys: String, CodingKey {
        case id, name, balance
        case availableBalance = "available_balance"
        case currency
    }
}
