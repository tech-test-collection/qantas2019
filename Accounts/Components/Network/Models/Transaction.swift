// Transaction.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let transaction = try? newJSONDecoder().decode(Transaction.self, from: jsonData)

import Foundation

// MARK: - Transaction
struct Transaction: Codable {
    let id, created: String
    let transactionDescription: String?
    let amount: Int
    let currency: String
    let merchant: Merchant?
    let amountIsPending: Bool
    let fees: Fees?

    enum CodingKeys: String, CodingKey {
        case id, created
        case transactionDescription = "description"
        case amount, currency, merchant
        case amountIsPending = "amount_is_pending"
        case fees
    }
}
