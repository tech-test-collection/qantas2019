// TransactionResponse.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let transactionResponse = try? newJSONDecoder().decode(TransactionResponse.self, from: jsonData)

import Foundation

// MARK: - TransactionResponse
struct TransactionResponse: Codable {
    let transactions: [Transaction]
}
