//
//  TransactionDetailsViewModel.swift
//  Accounts
//
//  Created by Mitchell Currie on 16/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

struct TransactionDetailsViewModel {
    // Own Fields
    let id: String
    let created: String
    let amount: String
    let currency: String
    let status: String
    let description: String
    // Merchant Fields
    let merchantName: String
    let merchantUpdated: String
    // Merchant.Address Fields
    let merchantAddress: String
    let merchantAddressCity: String
    let merchantAddressRegion: String
    let merchantAddressPostcode: String
    let merchantGeolocation: String
    
}

extension TransactionDetailsViewModel {
    init(transaction: Transaction) {
        id = transaction.id
        created = transaction.created
        amount = "$\(abs(transaction.amount))"
        currency = transaction.currency
        status = transaction.amountIsPending ? "Pending" : "Processed"
        description = transaction.transactionDescription ?? ""
        merchantName = transaction.merchant?.name ?? ""
        merchantUpdated = transaction.merchant?.updated ?? ""
        merchantAddress = transaction.merchant?.address?.address ?? ""
        merchantAddressCity = transaction.merchant?.address?.city ?? ""
        merchantAddressRegion = transaction.merchant?.address?.region ?? ""
        merchantAddressPostcode = transaction.merchant?.address?.postcode ?? ""
        // These two should go together always
        if let latitude = transaction.merchant?.address?.latitude,
            let longitude = transaction.merchant?.address?.longitude {
            merchantGeolocation = String(format: "(%0.6f, %0.6f)", latitude, longitude)
        } else {
            merchantGeolocation = ""
        }
    }
}

extension TransactionDetailsViewModel    {
    func viewWillAppear() {}
    func viewDidAppear() {}
    func viewWillDisappear() {}
    func viewDidDisappear() {}
}
