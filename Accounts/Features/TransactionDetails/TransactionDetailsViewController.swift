//
//  TransactionDetailsViewController.swift
//  Accounts
//
//  Created by Mitchell Currie on 16/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import UIKit

class TransactionDetailsViewController: UIViewController {
    @IBOutlet weak var detailsStack: UIStackView!
    @IBOutlet var labelCollection: [UILabel]!
    
    var viewModel: TransactionDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.viewWillAppear()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel?.viewDidDisappear()
    }
}

extension TransactionDetailsViewController {
    func bindViewModel() {
        guard let viewModel = viewModel else { return }
        // I'm just saving time here, it's not particularly pretty but shows all fields easily
        labelCollection[0].text = "ID: \(viewModel.id)"
        labelCollection[1].text = "Created: \(viewModel.created)"
        labelCollection[2].text = "Amount: \(viewModel.amount)"
        labelCollection[3].text = "Currency: \(viewModel.currency)"
        labelCollection[4].text = "Status: \(viewModel.status)"
        labelCollection[5].text = "Description:\n \(viewModel.description)"
        labelCollection[6].text = "Name: \(viewModel.merchantName)"
        labelCollection[7].text = "Last Updated: \(viewModel.merchantUpdated)"
        labelCollection[8].text = "Address: \(viewModel.merchantAddress)"
        labelCollection[9].text = "City: \(viewModel.merchantAddressCity)"
        labelCollection[10].text = "Region: \(viewModel.merchantAddressRegion)"
        labelCollection[11].text = "Postcode: \(viewModel.merchantAddressPostcode)"
        labelCollection[12].text = "Geolocation: \(viewModel.merchantGeolocation)"
    }
}
