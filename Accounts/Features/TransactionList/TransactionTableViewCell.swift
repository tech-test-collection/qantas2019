//
//  TransactionTableViewCell.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var merchantLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
}

extension TransactionTableViewCell {
    func setup(transaction: TransactionViewModel) {
        merchantLabel.text = transaction.merchant
        amountLabel.text = transaction.amount
        descriptionLabel.text = transaction.description
        statusImage.isHidden = !transaction.showsPendingImage
    }
}
