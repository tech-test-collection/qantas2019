//
//  TransactionViewModel.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

struct TransactionViewModel {
    let merchant: String
    let amount: String
    let description: String
    let showsPendingImage: Bool
    let details: TransactionDetailsViewModel
}

extension TransactionViewModel {
    init(transaction: Transaction) {
        merchant = transaction.merchant?.name ?? ""
        amount = "$\(abs(transaction.amount))"
        description = transaction.transactionDescription ?? ""
        showsPendingImage = transaction.amountIsPending
        details = TransactionDetailsViewModel(transaction: transaction)
    }
}
