//
//  TransactionListViewController.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import UIKit

class TransactionListViewController: UITableViewController {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var footerText: UILabel!
    
    var viewModel = TransactionListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }

}

extension TransactionListViewController: TransactionListViewModelDelegate {
    func footerTextChanged() {
        footerText.text = viewModel.footerText
    }
    
    func isLoadingChanged() {
        if viewModel.isLoading {
            loadingIndicator.startAnimating()
        } else {
            loadingIndicator.stopAnimating()
        }
    }
    
    func accountListUpdated() {
        tableView.reloadData()
    }
}

extension TransactionListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.transactionViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transaction = viewModel.transactionViewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: transaction.reuseIdentifier, for: indexPath)
        (cell as? TransactionTableViewCell)?.setup(transaction: transaction)
        return cell
    }
}

// Add the re-use identifiers as non-magic numbers, but the view moel doesn't know about the UI rendering
extension TransactionViewModel {
    var reuseIdentifier: String { return "transaction" }
}

extension TransactionListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsViewController = segue.destination as? TransactionDetailsViewController,
            let transactionCell = sender as? TransactionTableViewCell,
            let row = tableView.indexPath(for: transactionCell)?.row {
            detailsViewController.viewModel = viewModel.transactionViewModels[row].details
        }
    }
}
