//
//  AccountViewModel.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

struct AccountViewModel {
    let name: String
    let balance: String
}

extension AccountViewModel {
    init(account: Account) {
        self.name = account.name
        self.balance = "\(account.balance) (available: \(account.availableBalance))"
    }
}
