//
//  AccountListViewModel.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

protocol AccountListViewModelDelegate: AnyObject {
    func accountListUpdated()
    func isLoadingChanged()
    func footerTextChanged()
}

class AccountListViewModel {
    private enum State {
        case initial, loading, loaded, errored
    }
    weak var delegate: AccountListViewModelDelegate?
    var accountViewModels = [AccountViewModel]() {
        didSet {
            self.delegate?.accountListUpdated()
        }
    }
    var isLoading = false {
        didSet {
            self.delegate?.isLoadingChanged()
        }
    }
    var footerText: String? {
        didSet {
            self.delegate?.footerTextChanged()
        }
    }
    
    private var apiClient = SimpleAPIClient(environment: .demo)
    private var state = State.initial {
        didSet {
            switch state {
            case .initial:
                self.isLoading = false
                self.footerText = nil
            case .loading:
                self.isLoading = true
                self.footerText = "Loading accounts"
            case .loaded:
                self.isLoading = false
                self.footerText = nil
            case .errored:
                self.isLoading = false
                self.footerText = "Sorry, something went wrong"
            }
        }
    }
}

extension AccountListViewModel {
    func viewWillAppear() {
        self.state = .loading
        apiClient.fetchAccounts { [weak self] result in
            guard let result = try? result.get() else {
                self?.state = .errored
                return
            }
            self?.accountViewModels = result.accounts.map({ account in
                return AccountViewModel(account: account)
            })
            self?.state = .loaded
        }
    }
    func viewDidAppear() {}
    func viewWillDisappear() {}
    func viewDidDisappear() {}
}
