//
//  ViewController.swift
//  Accounts
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import UIKit

class AccountListViewController: UITableViewController {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var footerText: UILabel!
    
    var viewModel = AccountListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }

}

extension AccountListViewController: AccountListViewModelDelegate {
    func footerTextChanged() {
        footerText.text = viewModel.footerText
    }
    
    func isLoadingChanged() {
        if viewModel.isLoading {
            loadingIndicator.startAnimating()
        } else {
            loadingIndicator.stopAnimating()
        }
    }
    
    func accountListUpdated() {
        tableView.reloadData()
    }
}

extension AccountListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.accountViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let account = viewModel.accountViewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: account.reuseIdentifier, for: indexPath)
        cell.setup(account: account)
        return cell
    }
}

// Add the re-use identifiers as non-magic numbers, but the view moel doesn't know about the UI rendering
extension AccountViewModel {
    var reuseIdentifier: String { return "account" }
}

// We didn't need a subclass here, but that doesn't mean cellForRowAt should do the heavy lifting
extension UITableViewCell {
    func setup(account: AccountViewModel) {
        textLabel?.text = account.name
        detailTextLabel?.text = account.balance
    }
}
