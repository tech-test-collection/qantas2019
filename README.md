# Qantas2019

Qantas Loyalty coding assignment December 2019.

## Part 4 Answer 

#### Architecture

I chose the MVVM approach because:
* I'm very familiar with it (biased, but practical implications)
* Very popular these days
* Leads to straight forward code that is easy to debug and read/maintain

#### Libraries

I chose only one library:
* Alamofire

Alamofire was chosen because it takes out some of the boring boiler plate that is not at the heart of the product you are trying to build but merely a necessity.
The framework is incredibly popular, having reached and surpassed critical mass, such that building something of the same specifications yourself is probably not worth it unless it isn't right for you.
Further to that, not only is Alamofire well tested itself, but contains a great testing framework that it is itself well tested, because who tests the code that you write for your test cases. 


#### Other notes

Normally I'd include more tests than I did, however the recommended time and available time was smaller for this one, also it wasn't strictly mentioned, but of course I did include both tests and an approach that allows testing. Had I tested the API and mocks more extensively I would use OHTTPStubs.
Likewise, had more time been available it would look prettier, but it's servicable, and a good exemplar of MVVM, clean/modern swift code.
I choise iOS 13 as the target because it wasn't feasible to commiting to testing on more/lower targets, so therefore I did not pretend to support it (although it would probably work apart from the SF Symbol I used.)

Lastly, error handling and showing is light, it acknowledges failure to load but due to time I did not test it extensively, based on common experience I made the JSON model loading fairly generous in terms of null fields.

## Original Spec


**Qantas Money iOS Homework Task**

First of all thankyou for completing this homework task. We appreciate it takes time out of your day. 

We expect you to spend approximately 1 - 2 hours on this task. We’re looking for good knowledge of Apple’s APIs, and pragmatic, simple code that is correct, maintainable and easy to understand. Do not spend too much time on making the UI pretty You are free to use any third party libraries / dependency tools if you wish. If you don’t complete parts 1-3 please still do part 4. 

When you’re finished please submit your code to a public github/gitlab repository and send us the link. 

**Part 1.**

https://jsonstorage.net/api/items/d97f27b5-caba-4cc8-9f5d-32b0208ec7f0

Please create an app that shows a list of accounts. Each account view should show : 

- account name
- balance
- available balance.

**Part 2.**

https://jsonstorage.net/api/items/7a8a340d-b450-4adc-bbba-f6b4c8cdbc09

When an account is tapped, display a list of the transactions grouped by date (show the date as a header). Each transaction view should show the following: 

- Transaction description
- Transaction amount
- Transaction merchant name
- An indicator of whether the transaction is pending or not

**Part 3**

When tapping on one of these transactions show a details screen that displays ALL of the available information for each transaction. The layout and design is completely up to you. 

**Part 4**

Please create a short README file of why you took the approach you did, any problems you had and how you might address them if you spent more time. If you used any third party libraries please explain why. 
