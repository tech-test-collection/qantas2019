//
//  AccountTests.swift
//  AccountsTests
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import XCTest

class AccountTests: XCTestCase {
    func testAccountNormal() {
        do {
            let data = try StubFile.accountNormal.data()
            let account = try JSONDecoder().decode(Account.self, from: data)
            XCTAssertEqual(account.id, 9231412341424)
            XCTAssertEqual(account.name, "John Smith Savings Account")
            XCTAssertEqual(account.balance, 129.33)
            XCTAssertEqual(account.availableBalance, 33.99)
            XCTAssertEqual(account.currency, "GBP")
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
}
