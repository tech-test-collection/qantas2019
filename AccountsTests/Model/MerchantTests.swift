//
//  MerchantTests.swift
//  AccountsTests
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import XCTest

class MerchantTests: XCTestCase {
    func testMerchantNormal() {
        do {
            let data = try StubFile.merchantNormal.data()
            let merchant = try JSONDecoder().decode(Merchant.self, from: data)
            XCTAssertEqual(merchant.name, "Waitrose & Partners")
            XCTAssertEqual(merchant.updated, "2019-02-01T12:35:51.257Z")
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
    func testMerchantMinimal() {
        do {
            let data = try StubFile.merchantMinimal.data()
            let merchant = try JSONDecoder().decode(Merchant.self, from: data)
            XCTAssertNil(merchant.name)
            XCTAssertNil(merchant.updated)
            XCTAssertNil(merchant.address)
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
}
