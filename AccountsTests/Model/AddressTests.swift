//
//  AddressTests.swift
//  AccountsTests
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import XCTest

class AddressTests: XCTestCase {
    func testAddressNormal() {
        do {
            let data = try StubFile.addressNormal.data()
            let address = try JSONDecoder().decode(Address.self, from: data)
            XCTAssertEqual(address.address, "50 Victoria Street")
            XCTAssertEqual(address.city, "London")
            XCTAssertEqual(address.region, "Greater London")
            XCTAssertEqual(address.country, "GBR")
            XCTAssertEqual(address.postcode, "SW1H 0TL")
            XCTAssertEqual(address.latitude, 51.497792999999987)
            XCTAssertEqual(address.longitude, -0.135184)
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
    
    func testAddressMinimal() {
        do {
            let data = try StubFile.addressMinimal.data()
            let address = try JSONDecoder().decode(Address.self, from: data)
            XCTAssertNil(address.address)
            XCTAssertNil(address.city)
            XCTAssertNil(address.region)
            XCTAssertNil(address.country)
            XCTAssertNil(address.postcode)
            XCTAssertNil(address.latitude)
            XCTAssertNil(address.longitude)
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
}
