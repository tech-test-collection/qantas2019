//
//  XCTestExtensions.swift
//  AccountsTests
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import Foundation

enum StubFile: String {
    case accountNormal = "account-normal"
    case addressNormal = "address-normal"
    case addressMinimal = "address-minimal"
    case transactionNormal = "transaction-normal"
    case transactionMinimal = "transaction-minimal"
    case merchantNormal = "merchant-normal"
    case merchantMinimal = "merchant-minimal"
}

enum TestError: Error {
    case missingData
    case invalidData
    
    var localizedDescription: String {
        switch self {
        case .missingData: return "Data could not be found"
        case .invalidData: return "Data could not not be loaded"
        }
    }
}

// We can't find a bundle for class and this beats hard-coding it. Add in helper methods later and move me if you like
class TestHelper {}

extension StubFile {
    func data() throws -> Data {
        guard let url = Bundle(for: TestHelper.self).url(forResource: self.rawValue, withExtension: "json") else {
            throw TestError.missingData
        }
        do {
            return try Data(contentsOf: url)
        }
        catch {
            throw TestError.invalidData
        }
    }
}
