//
//  TransactionTests.swift
//  AccountsTests
//
//  Created by Mitchell Currie on 15/12/19.
//  Copyright © 2019 Mitchell Currie. All rights reserved.
//

import XCTest

class TransactionTests: XCTestCase {
    func testTransactionNormal() {
        do {
            let data = try StubFile.transactionNormal.data()
            let transaction = try JSONDecoder().decode(Transaction.self, from: data)
            XCTAssertEqual(transaction.id, "1")
            XCTAssertEqual(transaction.created, "2019-02-01T12:35:51.257Z")
            XCTAssertEqual(transaction.transactionDescription, "WAITROSE               HAMPTON       GBR")
            XCTAssertEqual(transaction.amount, -394)
            XCTAssertEqual(transaction.currency, "GBP")
            XCTAssertEqual(transaction.amountIsPending, true)
            XCTAssertNotNil(transaction.merchant)
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
    func testTransactionMinimal() {
        do {
            let data = try StubFile.transactionMinimal.data()
            let transaction = try JSONDecoder().decode(Transaction.self, from: data)
            XCTAssertEqual(transaction.id, "1")
            XCTAssertEqual(transaction.created, "2019-02-01T12:35:51.257Z")
            XCTAssertNil(transaction.transactionDescription)
            XCTAssertEqual(transaction.amount, -394)
            XCTAssertEqual(transaction.currency, "GBP")
            XCTAssertEqual(transaction.amountIsPending, true)
            XCTAssertNil(transaction.merchant)
        } catch let error {
            XCTFail("Unexpected error \(error.localizedDescription)")
        }
    }
}
